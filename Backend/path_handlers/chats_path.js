//internal dependencies
import {
  GetChats,
  inserting_chats_into_database,
} from "../utilities/mongoDb.js";
/**
 * chats_handler function calls the inserting chats into database function to inser data in database
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
const chats_handler = (request, response) => {
  const request_params = request.body;
  inserting_chats_into_database(request_params);
};

/**
 * get chats functions calls get chats function and get chats details from database
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
const get_chats_handler = (request, response) => {
  GetChats(response);
};

export { chats_handler, get_chats_handler };
