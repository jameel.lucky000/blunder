// defining the conflict_message it contains the object with respect to status_code, status_message
var conflict_message = {
  status_code: 409,
  status_message: "Conflict",
};

/**
 *
 * @param {1} username it contains username that users are trying to login
 * @returns a object that contains status_code and status_message
 */
function user_alredy_login(username) {
  return {
    status_code: 406,
    status_message: `Already logged in as ${username}`,
  };
}

// exporting the conflict_message, user_alredy_login
export { conflict_message, user_alredy_login };
