1. api 1
path : /register
method :POST
Description :it validate user data and push data into db

2.api 2
method :POST
path : /login
Description :it validate the user credentials

3. api 3
path : /forget_password
method : POST
Description :if the user forget the password it will reset the password

4. api 4
path : /match_requests
method : GET
Description : to get the match requests received for the user.

5. api 5
path : /
method : GET
Description : to get the home page.

6. api 6
path : /chats
method : GET
Description : to get the chats page.

7. api 7
path : /editprofile
method : PUT
Description : to update the user profile details.


