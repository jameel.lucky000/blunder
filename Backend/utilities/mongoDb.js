// importing mongoose from mongoose
import mongoose from "mongoose";

// importing the internal modules
import { UserRegisterSchema, UserLoginSchema, ChatsSchema } from "./Schema.js";

// conneting to database
mongoose.connect(
  "mongodb+srv://nmanchiravula:Nishitha1997@cluster0.j8eac.mongodb.net/Blunder?retryWrites=true&w=majority"
);

/* creating a model for the user_register_data for creating a list and enter objects in it */
const UserRegisterData = mongoose.model("UserRegisterData", UserRegisterSchema);

/* creating a model for the user_login_data for storing the user login details */
const UserLoginData = mongoose.model("UserLoginData", UserLoginSchema);

/* creating a model for the chats  for storing the chats details */
const ChatsData = mongoose.model("ChatsData", ChatsSchema);

/**
 * Inserting the user registering data into the database
 * @param user_data database contains the user registered data
 */
function inserting_register_data_into_database(user_data) {
  const UserData = new UserRegisterData({
    name: user_data.name,
    username: user_data.username,
    password: user_data.password,
    re_enter_password: user_data.re_enter_password,
    email: user_data.email,
    age: user_data.age,
    phone_no: user_data.phone_no,
    security_question: user_data.security_question,
    security_answer: user_data.security_answer,
    image: user_data.image,
  });
  // saving the data
  UserData.save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

/**
 * Inserting the user login data into the database
 * @param login_data database contains the user logged in data
 */
function inserting_user_login_data_into_database(login_data) {
  const UserLogin = new UserLoginData({
    username: login_data.username,
    password: login_data.password,
    // jwt_token: login_data.jwt_token,
  });
  // saving the data
  UserLogin.save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

/**
 * inserting data into database function takes data params
 * create new chats
 * push the data into the database of the chatsSchema
 * @param {data} to pass the arguments from the requested body
 */
function inserting_chats_into_database(data) {
  const chats_data = new ChatsData({
    name: data.name,
    profilePic: data.profilePic,
    message: data.message,
    timestamp: data.timestamp,
  });
  chats_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

/**
 * Getchats function fetch all the chats or retuns error if any
 */
function GetChats(response) {
  ChatsData.find({}, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      response.send(data);
    }
  });
}

export {
  UserLoginData,
  UserRegisterData,
  ChatsData,
  inserting_register_data_into_database,
  inserting_user_login_data_into_database,
  inserting_chats_into_database,
  GetChats,
};
