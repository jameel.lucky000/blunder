// External Dependencies
import React, { Component } from "react";
import Loader from "react-loader-spinner";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
// Internal Dependecies
import "./Chats.css";
import Chat from "./Chat";

// const ChatsDetails = [
//   {
//     id: 1,
//     name: "Sean Rad",
//     message: "👋 Hey ! how are you ?",
//     timestamp: "8 minutes ago",
//     profilePic:
//       "https://i.dailymail.co.uk/i/pix/2015/11/18/20/2E92DB7E00000578-3324317-image-a-52_1447877890119.jpg",
//   },
//   {
//     id: 2,
//     name: "Elon Musk",
//     message: "Moving to mars straight from 🌎",
//     timestamp: "12 seconds ago",
//     profilePic:
//       "https://upload.wikimedia.org/wikipedia/commons/e/ed/Elon_Musk_Royal_Society.jpg",
//   },
//   {
//     id: 3,
//     name: "Drew Mcintyre",
//     message: "Are you ready the this match ? 😠",
//     timestamp: "9 days ago",
//     profilePic:
//       "https://cdn.vox-cdn.com/thumbor/yV2UokvVJSUCwuEYnituJdEc22U=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/22665592/1045323484.jpg",
//   },
//   {
//     id: 4,
//     name: "Jack Dorsey",
//     message: "I think Trump would be the next president 🏆",
//     timestamp: "40 seconds ago",
//     profilePic:
//       "https://ichef.bbci.co.uk/news/976/cpsprodpb/67E4/production/_121869562_hi072294801.jpg",
//   },
// ];

//declaring chats component
class Chats extends Component {
  state = {
    searchInput: "",
    chatsDetailsList: [],
  };

  componentDidMount() {
    this.getChatsData();
  }

  onChangeSearchInput = (event) => {
    this.setState({
      searchInput: event.target.value,
    });
  };

  getChatsData = async () => {
    const response = await fetch("http://localhost:8000/chats");
    const statusCode = await response.statusCode;
    console.log(statusCode);
    const data = await response.json();

    const formattedData = data.map((eachItem) => ({
      name: eachItem.name,
      message: eachItem.message,
      timestamp: eachItem.timestamp,
      profilePic: eachItem.profilePic,
    }));
    this.setState({ chatsDetailsList: formattedData });
  };
  render() {
    const { searchInput, chatsDetailsList } = this.state;
    const searchResults = chatsDetailsList.filter((eachUser) =>
      eachUser.name.includes(searchInput)
    );
    return (
      <>
        <div className="search-container">
          <input
            class="icon-rtl"
            placeholder="Search"
            type="search"
            onChange={this.onChangeSearchInput}
            value={searchInput}
          />
        </div>
        <div className="chats">
          {searchResults.map((eachchat) => (
            <Chat key={eachchat.id} chat={eachchat} />
          ))}
        </div>
      </>
    );
  }
}

// Exporting the above Chats function.
export default Chats;
