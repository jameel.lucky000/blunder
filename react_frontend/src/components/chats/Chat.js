// External Dependencies
import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { Link } from "react-router-dom";
import { Component } from "react";
// Internal Dependecies
import "./Chat.css";

//declaring the chat component
class Chat extends Component {
  render() {
    const { chat } = this.props;
    const { name, message, timestamp, profilePic } = chat;
    return (
      <Link to={`/chats/${name}`}>
        <div className="chat">
          <Avatar className="chat__image" src={profilePic} />
          <div className="chat__details">
            <h2>{name}</h2>
            <p>{message}</p>
          </div>
          <p className="chat__timestamp">{timestamp}</p>
        </div>
      </Link>
    );
  }
}

// Exporting the above Chat function.
export default Chat;
